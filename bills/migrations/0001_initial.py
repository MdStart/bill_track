# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-04-18 19:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bill',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(verbose_name='Bill Name')),
                ('due_date', models.DateField(verbose_name='Bill Due Date')),
                ('is_recurring', models.BooleanField(verbose_name='Recurring')),
            ],
        ),
    ]
