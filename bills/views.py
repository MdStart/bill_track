from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils import timezone
from django.views import generic

from bills.models import Bill
from bills.forms import BillCreateForm


class UnpaidBillListView(LoginRequiredMixin, generic.ListView):
    model = Bill
    template_name = 'bills/unpaid_bill_list.html'

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user, is_paid=False).order_by('due_date')

    def get_context_data(self):
        context = super().get_context_data()
        context['today'] = timezone.now().date()
        return context


class BillListView(LoginRequiredMixin, generic.ListView):
    model = Bill

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user).order_by('due_date')

    def get_context_data(self):
        context = super().get_context_data()
        context['today'] = timezone.now().date()
        return context


class BillDetailView(LoginRequiredMixin, generic.DetailView):
    model = Bill

    def get_queryset(self):
        # Ensures users can only see their own bills
        return super().get_queryset().filter(user=self.request.user)


class BillCreateView(LoginRequiredMixin, generic.CreateView):
    model = Bill
    form_class = BillCreateForm

    def form_valid(self, form):
        # Sets bill owner to logged in user
        form.instance.user = self.request.user
        return super().form_valid(form)
