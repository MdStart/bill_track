import uuid

from django.urls import reverse
from django.contrib.auth.models import User
from django.db import models


class Bill(models.Model):
    user = models.ForeignKey(User, null=True)
    slug = models.UUIDField(default=uuid.uuid4, editable=False)

    name = models.TextField(verbose_name="Bill Name")
    due_date = models.DateField(verbose_name="Bill Due Date")
    is_paid = models.BooleanField(verbose_name="Bill Paid", default=False)

    def get_absolute_url(self):
        return reverse('bill-detail', kwargs={'slug': self.slug})
